import express from 'express'
import passport from 'passport'
const router = express.Router()

router.get('/google', passport.authenticate('google', { scope: ['profile'] }))

router.get(
  '/google/auth',
  passport.authenticate('google', { failureRedirect: '/' }),
  (req, res) => {
      console.log('Hello im in herer')
    res.send('Success')
  }
)

export default router
