import mongoose from 'mongoose'

const connectDB = async () => {
  try {
    await mongoose.connect('mongodb://localhost:27017/google', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    console.log('Connected To DB')
  } catch (error) {
    console.log('Error while connecting.....')
  }
}

export default connectDB
