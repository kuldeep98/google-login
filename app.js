import express from 'express'
import dotenv from 'dotenv'
import passport from 'passport'
import pass from './passport.js'
import session from 'express-session'
import auth from './auth.js'
import connectDB from './db.js'
dotenv.config()

connectDB()

pass(passport)

const app = express()
app.use(
  session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 20000 },
  })
)

app.use(passport.initialize())
app.use(passport.session())

app.use('/', auth)
app.get(
  '/home',
  (req, res, next) => {
    if (!req.session.passport) {
      res.redirect('/')
    } else {
      next()
    }
  },
  (req, res) => {
    res.send('Hello You are Loged In')
  }
)
const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
  console.log('Server started at ' + PORT)
})
